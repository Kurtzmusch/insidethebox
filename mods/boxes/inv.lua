
--[[

  ITB (insidethebox) minetest game - Copyright (C) 2017-2018 sofar & nore

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public License
  as published by the Free Software Foundation; either version 2.1
  of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free
  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
  MA 02111-1307 USA

]]--

local function has_infinite_items(player)
	local name = player:get_player_name()
	if boxes.players_editing_boxes[name] then
		return true
	end
	if boxes.players_in_boxes[name] then
		return false
	end
	return minetest.check_player_privs(name, "server")
end

-- Don't take item if editing a box
minetest.register_on_placenode(function(pos, newnode, placer, oldnode, itemstack, pointed_thing)
		if not placer then return false end
		return has_infinite_items(placer)
end)

function minetest.handle_node_drops(pos, drops, digger)
	if not digger or not digger:is_player() then
		return
	end
	local always_add = not has_infinite_items(digger)
	local inv = digger:get_inventory()
	if inv then
		for _, item in ipairs(drops) do
			item = ItemStack(item):get_name()
			if always_add or not inv:contains_item("main", item) then
				inv:add_item("main", item)
			end
		end
	end
end
